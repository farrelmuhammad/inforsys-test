import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import { Avatar, Box, Button, Checkbox, createTheme, CssBaseline, FormControlLabel, Grid, Paper, TextField, ThemeProvider, Typography } from '@mui/material'
// import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import Link from 'next/link'
import { useState } from 'react'
import headerLogin from '../assets/header-login.png'
import headerSplash from '../assets/header-splash.png'
import Logo from '../assets/logo.png'
import Swal from 'sweetalert2'
import axios from 'axios'

function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const theme = createTheme();

export default function Home() {
  const [formData, setFormData] = useState({
    username: '',
    password: ''
  })

  const handleSubmit = (e) => {
    e.preventDefault();
    fetch('https://dummyjson.com/auth/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({

        username: 'kminchelle',
        password: '0lelplR',
        // expiresInMins: 60, // optional
      })
    })
    // axios({
    //   method: "post",
    //   url: 'https://dummyjson.com/auth/login',
    //   body: JSON.stringify(formData),
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/x-www-form-urlencoded",
    //   },
    // })
    //   .then((res) => {
    //     console.log(res);
    //     Swal.fire({
    //       // title: "Berhasil!",
    //       text: "Login Success!",
    //       icon: "success",
    //       showConfirmButton: false,
    //       timer: 1000,
    //       timerProgressBar: true,
    //       didOpen: (toast) => {
    //         toast.addEventListener("mouseenter", Swal.stopTimer);
    //         toast.addEventListener("mouseleave", Swal.resumeTimer);
    //       },
    //     });
    //     // navigate("/");
    //   })
    //   .catch((err) => {
    //     if (err.response) {
    //       console.log("err.response ", err.response);
    //     } else if (err.request) {
    //       console.log("err.request ", err.request);
    //     } else if (err.message) {
    //       // do something other than the other two
    //     }
    //   });
    // console.log({
    //   email: data.get('email'),
    //   password: data.get('password'),
    // });
  };
  return (
    <>
      <ThemeProvider theme={theme}>
        <Grid container component="main" sx={{ height: '100vh' }}>
          <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square
          // sx={{ backgroundImage: `url(${headerLogin.src})` }}
          >
            <Box
              sx={{
                my: 8,
                mx: 4,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              {/* <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}> */}
              <Image src={Logo} alt="header login" width={100} height={60} />
              {/* </Avatar> */}
            </Box>
            <Box
              sx={{
                my: 8,
                mx: 4,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'start',
              }}
            >
              <Typography component="h1" variant="h4">
                Login
              </Typography>
              <Typography component="h1" variant="h6">
                Please sign in to continue.
              </Typography>
              <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
                <TextField
                  margin="normal"
                  // required
                  fullWidth
                  id="email"
                  label="User ID"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  variant='standard'
                  onChange={(e) => setFormData({ ...formData, username: e.target.value })}
                />
                <TextField
                  margin="normal"
                  // required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  variant='standard'
                  onChange={(e) => setFormData({ ...formData, password: e.target.value })}
                />
                {/* <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"
                /> */}
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2, borderRadius: 10, width: '30%', backgroundColor: '#3f51b5' }}
                  onClick={() => handleSubmit(formData)}
                >
                  LOGIN
                </Button>
                <Grid container sx={{ marginTop: '5rem' }}>
                  <Grid item>
                    <Link href="#" variant="body2">
                      {"Don't have an account? Sign Up"}
                    </Link>
                  </Grid>
                </Grid>
                {/* <Copyright sx={{ mt: 5 }} /> */}
              </Box>
            </Box>
          </Grid>
          <CssBaseline />
          <Grid
            item
            xs={false}
            sm={4}
            md={7}
            sx={{
              // backgroundImage: 'url(https://source.unsplash.com/random)',
              backgroundImage: `url(${headerSplash.src})`,
              backgroundRepeat: 'no-repeat',
              backgroundColor: (t) =>
                t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
              backgroundSize: 'cover',
              backgroundPosition: 'center',
            }}
          />

        </Grid>
      </ThemeProvider>
    </>
  )
}
